<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="EC20">
<packages>
<package name="EC20" urn="urn:adsk.eagle:footprint:22384/1" locally_modified="yes">
<description>&lt;b&gt;EC20&lt;/b&gt;</description>
<wire x1="-20.59" y1="7.025" x2="-20.59" y2="8.295" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="8.295" x2="-19.955" y2="8.93" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="8.93" x2="-18.685" y2="8.93" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="8.93" x2="-18.05" y2="8.295" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="8.93" x2="-20.59" y2="9.565" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="9.565" x2="-20.59" y2="10.835" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="10.835" x2="-19.955" y2="11.47" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="11.47" x2="-18.05" y2="10.835" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="10.835" x2="-18.05" y2="9.565" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="9.565" x2="-18.685" y2="8.93" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="3.85" x2="-20.59" y2="4.485" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="4.485" x2="-20.59" y2="5.755" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="5.755" x2="-19.955" y2="6.39" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="6.39" x2="-18.685" y2="6.39" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="6.39" x2="-18.05" y2="5.755" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="5.755" x2="-18.05" y2="4.485" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="4.485" x2="-18.685" y2="3.85" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="7.025" x2="-19.955" y2="6.39" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="6.39" x2="-18.05" y2="7.025" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="8.295" x2="-18.05" y2="7.025" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-0.595" x2="-20.59" y2="0.675" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="0.675" x2="-19.955" y2="1.31" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="1.31" x2="-18.685" y2="1.31" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="1.31" x2="-18.05" y2="0.675" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="1.31" x2="-20.59" y2="1.945" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="1.945" x2="-20.59" y2="3.215" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="3.215" x2="-19.955" y2="3.85" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="3.85" x2="-18.685" y2="3.85" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="3.85" x2="-18.05" y2="3.215" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="3.215" x2="-18.05" y2="1.945" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="1.945" x2="-18.685" y2="1.31" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-3.97" x2="-20.59" y2="-3.135" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-3.135" x2="-20.59" y2="-1.865" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-1.865" x2="-19.955" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-1.23" x2="-18.685" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-1.23" x2="-18.05" y2="-1.865" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-1.865" x2="-18.05" y2="-3.135" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-3.135" x2="-18.685" y2="-3.97" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-0.595" x2="-19.955" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-1.23" x2="-18.05" y2="-0.595" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="0.675" x2="-18.05" y2="-0.595" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-8.515" x2="-20.59" y2="-7.245" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-7.245" x2="-19.955" y2="-6.61" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-6.61" x2="-18.685" y2="-6.61" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-6.61" x2="-18.05" y2="-7.245" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-6.61" x2="-20.59" y2="-5.875" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-5.875" x2="-20.59" y2="-4.605" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-4.605" x2="-19.955" y2="-3.97" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-3.97" x2="-18.685" y2="-3.97" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-3.97" x2="-18.05" y2="-4.605" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-4.605" x2="-18.05" y2="-5.875" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-5.875" x2="-18.685" y2="-6.61" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-11.89" x2="-20.59" y2="-11.355" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-11.355" x2="-20.59" y2="-10.085" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-10.085" x2="-19.955" y2="-9.25" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-9.25" x2="-18.685" y2="-9.25" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-9.25" x2="-18.05" y2="-9.985" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-9.985" x2="-18.05" y2="-11.255" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-11.255" x2="-18.685" y2="-11.89" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-8.515" x2="-19.955" y2="-9.25" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-9.25" x2="-18.05" y2="-8.515" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-7.245" x2="-18.05" y2="-8.515" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-14.53" x2="-18.685" y2="-14.53" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-14.53" x2="-20.59" y2="-13.795" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-13.795" x2="-20.59" y2="-12.525" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="-12.525" x2="-19.955" y2="-11.89" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="-11.89" x2="-18.685" y2="-11.89" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="-11.89" x2="-18.05" y2="-12.525" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-12.525" x2="-18.05" y2="-13.795" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="-13.795" x2="-18.685" y2="-14.53" width="0.1524" layer="21"/>
<pad name="DTR" x="-19.32" y="-2.5" drill="1.016" shape="long" rot="R180"/>
<pad name="TXD2" x="-19.32" y="0.04" drill="1.016" shape="long" rot="R180"/>
<pad name="RXD2" x="-19.32" y="2.58" drill="1.016" shape="long" rot="R180"/>
<pad name="VBAT" x="-19.32" y="5.12" drill="1.016" shape="long" rot="R180"/>
<pad name="NET" x="-19.32" y="7.66" drill="1.016" shape="long" rot="R180"/>
<pad name="VIN1" x="-19.32" y="10.2" drill="1.016" shape="long" rot="R180"/>
<text x="-2" y="-3" size="1.27" layer="25">EC20</text>
<pad name="VEXT" x="-19.32" y="-13.16" drill="1.016" shape="long" rot="R180"/>
<pad name="VTTL" x="-19.22" y="-10.62" drill="1.016" shape="long" rot="R180"/>
<pad name="CTS" x="-19.32" y="-7.88" drill="1.016" shape="long" rot="R180"/>
<pad name="RI" x="-19.32" y="-5.24" drill="1.016" shape="long" rot="R180"/>
<wire x1="21.69" y1="7.025" x2="21.69" y2="8.295" width="0.1524" layer="21"/>
<wire x1="21.69" y1="8.295" x2="22.325" y2="8.93" width="0.1524" layer="21"/>
<wire x1="22.325" y1="8.93" x2="23.595" y2="8.93" width="0.1524" layer="21"/>
<wire x1="23.595" y1="8.93" x2="24.23" y2="8.295" width="0.1524" layer="21"/>
<wire x1="22.325" y1="8.93" x2="21.69" y2="9.565" width="0.1524" layer="21"/>
<wire x1="21.69" y1="9.565" x2="21.69" y2="10.835" width="0.1524" layer="21"/>
<wire x1="21.69" y1="10.835" x2="22.325" y2="11.47" width="0.1524" layer="21"/>
<wire x1="22.325" y1="11.47" x2="23.595" y2="11.47" width="0.1524" layer="21"/>
<wire x1="23.595" y1="11.47" x2="24.23" y2="10.835" width="0.1524" layer="21"/>
<wire x1="24.23" y1="10.835" x2="24.23" y2="9.565" width="0.1524" layer="21"/>
<wire x1="24.23" y1="9.565" x2="23.595" y2="8.93" width="0.1524" layer="21"/>
<wire x1="22.325" y1="3.85" x2="21.69" y2="4.485" width="0.1524" layer="21"/>
<wire x1="21.69" y1="4.485" x2="21.69" y2="5.755" width="0.1524" layer="21"/>
<wire x1="21.69" y1="5.755" x2="22.325" y2="6.39" width="0.1524" layer="21"/>
<wire x1="22.325" y1="6.39" x2="23.595" y2="6.39" width="0.1524" layer="21"/>
<wire x1="23.595" y1="6.39" x2="24.23" y2="5.755" width="0.1524" layer="21"/>
<wire x1="24.23" y1="5.755" x2="24.23" y2="4.485" width="0.1524" layer="21"/>
<wire x1="24.23" y1="4.485" x2="23.595" y2="3.85" width="0.1524" layer="21"/>
<wire x1="21.69" y1="7.025" x2="22.325" y2="6.39" width="0.1524" layer="21"/>
<wire x1="23.595" y1="6.39" x2="24.23" y2="7.025" width="0.1524" layer="21"/>
<wire x1="24.23" y1="8.295" x2="24.23" y2="7.025" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-0.595" x2="21.69" y2="0.675" width="0.1524" layer="21"/>
<wire x1="21.69" y1="0.675" x2="22.325" y2="1.31" width="0.1524" layer="21"/>
<wire x1="22.325" y1="1.31" x2="23.595" y2="1.31" width="0.1524" layer="21"/>
<wire x1="23.595" y1="1.31" x2="24.23" y2="0.675" width="0.1524" layer="21"/>
<wire x1="22.325" y1="1.31" x2="21.69" y2="1.945" width="0.1524" layer="21"/>
<wire x1="21.69" y1="1.945" x2="21.69" y2="3.215" width="0.1524" layer="21"/>
<wire x1="21.69" y1="3.215" x2="22.325" y2="3.85" width="0.1524" layer="21"/>
<wire x1="22.325" y1="3.85" x2="23.595" y2="3.85" width="0.1524" layer="21"/>
<wire x1="23.595" y1="3.85" x2="24.23" y2="3.215" width="0.1524" layer="21"/>
<wire x1="24.23" y1="3.215" x2="24.23" y2="1.945" width="0.1524" layer="21"/>
<wire x1="24.23" y1="1.945" x2="23.595" y2="1.31" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-3.97" x2="21.69" y2="-3.135" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-3.135" x2="21.69" y2="-1.865" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-1.865" x2="22.325" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-1.23" x2="23.595" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-1.23" x2="24.23" y2="-1.865" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-1.865" x2="24.23" y2="-3.135" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-3.135" x2="23.595" y2="-3.97" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-0.595" x2="22.325" y2="-1.23" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-1.23" x2="24.23" y2="-0.595" width="0.1524" layer="21"/>
<wire x1="24.23" y1="0.675" x2="24.23" y2="-0.595" width="0.1524" layer="21"/>
<wire x1="21.79" y1="-8.715" x2="21.79" y2="-7.445" width="0.1524" layer="21"/>
<wire x1="21.79" y1="-7.445" x2="22.325" y2="-6.51" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-6.51" x2="23.595" y2="-6.51" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-6.51" x2="24.13" y2="-7.345" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-6.51" x2="21.69" y2="-5.975" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-5.975" x2="21.69" y2="-4.705" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-4.705" x2="22.325" y2="-3.97" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-3.97" x2="23.595" y2="-3.97" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-3.97" x2="24.23" y2="-4.705" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-4.705" x2="24.23" y2="-5.975" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-5.975" x2="23.595" y2="-6.51" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-11.89" x2="21.79" y2="-11.255" width="0.1524" layer="21"/>
<wire x1="21.79" y1="-11.255" x2="21.79" y2="-9.985" width="0.1524" layer="21"/>
<wire x1="21.79" y1="-9.985" x2="22.325" y2="-9.25" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-9.25" x2="23.595" y2="-9.25" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-9.25" x2="24.23" y2="-10.085" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-10.085" x2="24.23" y2="-11.355" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-11.355" x2="23.595" y2="-11.89" width="0.1524" layer="21"/>
<wire x1="21.79" y1="-8.715" x2="22.325" y2="-9.25" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-9.25" x2="24.13" y2="-8.615" width="0.1524" layer="21"/>
<wire x1="24.13" y1="-7.345" x2="24.13" y2="-8.615" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-16.535" x2="21.69" y2="-15.265" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-15.265" x2="22.325" y2="-14.63" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-14.63" x2="23.595" y2="-14.63" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-14.63" x2="24.23" y2="-15.365" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-14.63" x2="21.69" y2="-13.895" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-13.895" x2="21.69" y2="-12.625" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-12.625" x2="22.325" y2="-11.89" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-11.89" x2="23.595" y2="-11.89" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-11.89" x2="24.13" y2="-12.625" width="0.1524" layer="21"/>
<wire x1="24.13" y1="-12.625" x2="24.13" y2="-13.895" width="0.1524" layer="21"/>
<wire x1="24.13" y1="-13.895" x2="23.595" y2="-14.63" width="0.1524" layer="21"/>
<wire x1="22.325" y1="-17.17" x2="23.595" y2="-17.17" width="0.1524" layer="21"/>
<wire x1="21.69" y1="-16.535" x2="22.325" y2="-17.17" width="0.1524" layer="21"/>
<wire x1="23.595" y1="-17.17" x2="24.23" y2="-16.635" width="0.1524" layer="21"/>
<wire x1="24.23" y1="-15.365" x2="24.23" y2="-16.635" width="0.1524" layer="21"/>
<wire x1="22.325" y1="11.47" x2="21.69" y2="12.105" width="0.1524" layer="21"/>
<wire x1="21.69" y1="12.105" x2="21.69" y2="13.375" width="0.1524" layer="21"/>
<wire x1="21.69" y1="13.375" x2="22.325" y2="14.01" width="0.1524" layer="21"/>
<wire x1="22.325" y1="14.01" x2="23.595" y2="14.01" width="0.1524" layer="21"/>
<wire x1="23.595" y1="14.01" x2="24.23" y2="13.375" width="0.1524" layer="21"/>
<wire x1="24.23" y1="13.375" x2="24.23" y2="12.105" width="0.1524" layer="21"/>
<wire x1="24.23" y1="12.105" x2="23.595" y2="11.47" width="0.1524" layer="21"/>
<pad name="TXD" x="22.96" y="-2.5" drill="1.016" shape="long" rot="R180"/>
<pad name="RXD" x="22.96" y="0.04" drill="1.016" shape="long" rot="R180"/>
<pad name="RTS" x="22.96" y="2.58" drill="1.016" shape="long" rot="R180"/>
<pad name="PEN" x="22.96" y="5.12" drill="1.016" shape="long" rot="R180"/>
<pad name="RST" x="22.96" y="7.66" drill="1.016" shape="long" rot="R180"/>
<pad name="GND" x="22.96" y="10.2" drill="1.016" shape="long" rot="R180"/>
<pad name="P/G" x="22.96" y="12.74" drill="1.016" shape="long" rot="R180"/>
<pad name="SC" x="22.96" y="-15.9" drill="1.016" shape="long" rot="R180"/>
<pad name="SD" x="22.96" y="-13.16" drill="1.016" shape="long" rot="R180"/>
<pad name="SR" x="23.06" y="-10.62" drill="1.016" shape="long" rot="R180"/>
<pad name="SV" x="22.96" y="-7.88" drill="1.016" shape="long" rot="R180"/>
<pad name="STA" x="22.96" y="-5.24" drill="1.016" shape="long" rot="R180"/>
<wire x1="-22.86" y1="15.24" x2="-22.86" y2="-19.05" width="0.1524" layer="51"/>
<wire x1="-22.86" y1="-19.05" x2="26.67" y2="-19.05" width="0.1524" layer="51"/>
<wire x1="26.67" y1="-19.05" x2="26.67" y2="15.24" width="0.1524" layer="51"/>
<wire x1="26.67" y1="15.24" x2="-22.86" y2="15.24" width="0.1524" layer="51"/>
<wire x1="-19.955" y1="11.47" x2="-20.59" y2="12.105" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="12.105" x2="-20.59" y2="13.375" width="0.1524" layer="21"/>
<wire x1="-20.59" y1="13.375" x2="-19.955" y2="14.01" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="14.01" x2="-18.685" y2="14.01" width="0.1524" layer="21"/>
<wire x1="-18.685" y1="14.01" x2="-18.05" y2="13.375" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="13.375" x2="-18.05" y2="12.105" width="0.1524" layer="21"/>
<wire x1="-18.05" y1="12.105" x2="-18.685" y2="11.47" width="0.1524" layer="21"/>
<wire x1="-19.955" y1="11.47" x2="-18.685" y2="11.47" width="0.1524" layer="21"/>
<pad name="VIN" x="-19.32" y="12.74" drill="1.016" shape="long" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="EC20">
<pin name="SC" x="-15.24" y="12.7" length="middle"/>
<pin name="SD" x="-15.24" y="10.16" length="middle"/>
<pin name="SR" x="-15.24" y="7.62" length="middle"/>
<pin name="SV" x="-15.24" y="5.08" length="middle"/>
<pin name="STA" x="-15.24" y="2.54" length="middle"/>
<pin name="TXD" x="-15.24" y="0" length="middle"/>
<pin name="RXD" x="-15.24" y="-2.54" length="middle"/>
<pin name="RTS" x="-15.24" y="-5.08" length="middle"/>
<pin name="PEN" x="-15.24" y="-7.62" length="middle"/>
<pin name="RST" x="-15.24" y="-10.16" length="middle"/>
<pin name="GND" x="-15.24" y="-12.7" length="middle"/>
<pin name="VIN" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="VIN1" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="NET" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="VBAT" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="RXD2" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="TXD2" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="DTR" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="RI" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="CTS" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="VTTL" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="VEXT" x="17.78" y="12.7" length="middle" rot="R180"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="95">EC20</text>
<pin name="P/G" x="-15.24" y="-15.24" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EC20">
<gates>
<gate name="G$1" symbol="EC20" x="-7.62" y="2.54"/>
</gates>
<devices>
<device name="" package="EC20">
<connects>
<connect gate="G$1" pin="CTS" pad="CTS"/>
<connect gate="G$1" pin="DTR" pad="DTR"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="NET" pad="NET"/>
<connect gate="G$1" pin="P/G" pad="P/G"/>
<connect gate="G$1" pin="PEN" pad="PEN"/>
<connect gate="G$1" pin="RI" pad="RI"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="RTS" pad="RTS"/>
<connect gate="G$1" pin="RXD" pad="RXD"/>
<connect gate="G$1" pin="RXD2" pad="RXD2"/>
<connect gate="G$1" pin="SC" pad="SC"/>
<connect gate="G$1" pin="SD" pad="SD"/>
<connect gate="G$1" pin="SR" pad="SR"/>
<connect gate="G$1" pin="STA" pad="STA"/>
<connect gate="G$1" pin="SV" pad="SV"/>
<connect gate="G$1" pin="TXD" pad="TXD"/>
<connect gate="G$1" pin="TXD2" pad="TXD2"/>
<connect gate="G$1" pin="VBAT" pad="VBAT"/>
<connect gate="G$1" pin="VEXT" pad="VEXT"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VIN1" pad="VIN1"/>
<connect gate="G$1" pin="VTTL" pad="VTTL"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="hart2012h">
<packages>
<package name="HAR2012H" urn="urn:adsk.eagle:footprint:22384/1" locally_modified="yes">
<description>&lt;b&gt;EC20&lt;/b&gt;</description>
<wire x1="-22.09" y1="1.525" x2="-22.09" y2="2.795" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="2.795" x2="-21.455" y2="3.43" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="3.43" x2="-20.185" y2="3.43" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="3.43" x2="-19.55" y2="2.795" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="3.43" x2="-22.09" y2="4.065" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="4.065" x2="-22.09" y2="5.335" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="5.335" x2="-21.455" y2="5.97" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="5.97" x2="-19.55" y2="5.335" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="5.335" x2="-19.55" y2="4.065" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="4.065" x2="-20.185" y2="3.43" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-1.65" x2="-22.09" y2="-1.015" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-1.015" x2="-22.09" y2="0.255" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="0.255" x2="-21.455" y2="0.89" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="0.89" x2="-20.185" y2="0.89" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="0.89" x2="-19.55" y2="0.255" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="0.255" x2="-19.55" y2="-1.015" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="-1.015" x2="-20.185" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="1.525" x2="-21.455" y2="0.89" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="0.89" x2="-19.55" y2="1.525" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="2.795" x2="-19.55" y2="1.525" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-6.095" x2="-22.09" y2="-4.825" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-4.825" x2="-21.455" y2="-4.19" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-4.19" x2="-20.185" y2="-4.19" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="-4.19" x2="-19.55" y2="-4.825" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-4.19" x2="-22.09" y2="-3.555" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-3.555" x2="-22.09" y2="-2.285" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-2.285" x2="-21.455" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-1.65" x2="-20.185" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="-1.65" x2="-19.55" y2="-2.285" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="-2.285" x2="-19.55" y2="-3.555" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="-3.555" x2="-20.185" y2="-4.19" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-8.97" x2="-22.09" y2="-8.635" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-8.635" x2="-22.09" y2="-7.365" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-7.365" x2="-21.455" y2="-6.73" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-6.73" x2="-20.185" y2="-6.73" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="-6.73" x2="-19.55" y2="-7.365" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="-7.365" x2="-19.55" y2="-8.635" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="-8.635" x2="-20.185" y2="-8.97" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="-6.095" x2="-21.455" y2="-6.73" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="-6.73" x2="-19.55" y2="-6.095" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="-4.825" x2="-19.55" y2="-6.095" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="-8.97" x2="-20.185" y2="-8.97" width="0.1524" layer="21"/>
<pad name="GND" x="-20.82" y="-8" drill="1.016" shape="long" rot="R180"/>
<pad name="RTS" x="-20.82" y="-5.46" drill="1.016" shape="long" rot="R180"/>
<pad name="OCD" x="-20.82" y="-2.92" drill="1.016" shape="long" rot="R180"/>
<pad name="RST" x="-20.82" y="-0.38" drill="1.016" shape="long" rot="R180"/>
<pad name="TXD" x="-20.82" y="2.16" drill="1.016" shape="long" rot="R180"/>
<pad name="RXD" x="-20.82" y="4.7" drill="1.016" shape="long" rot="R180"/>
<text x="-4" y="-2" size="1.27" layer="25">HART2012</text>
<wire x1="19.19" y1="1.525" x2="19.19" y2="2.795" width="0.1524" layer="21"/>
<wire x1="19.19" y1="2.795" x2="19.825" y2="3.43" width="0.1524" layer="21"/>
<wire x1="19.825" y1="3.43" x2="21.095" y2="3.43" width="0.1524" layer="21"/>
<wire x1="21.095" y1="3.43" x2="22.23" y2="2.795" width="0.1524" layer="21"/>
<wire x1="19.825" y1="-1.65" x2="19.19" y2="-1.015" width="0.1524" layer="21"/>
<wire x1="19.19" y1="-1.015" x2="19.19" y2="0.255" width="0.1524" layer="21"/>
<wire x1="19.19" y1="0.255" x2="19.825" y2="0.89" width="0.1524" layer="21"/>
<wire x1="19.825" y1="0.89" x2="21.095" y2="0.89" width="0.1524" layer="21"/>
<wire x1="21.095" y1="0.89" x2="21.73" y2="0.255" width="0.1524" layer="21"/>
<wire x1="21.73" y1="0.255" x2="21.73" y2="-1.015" width="0.1524" layer="21"/>
<wire x1="21.73" y1="-1.015" x2="21.095" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="19.19" y1="1.525" x2="19.825" y2="0.89" width="0.1524" layer="21"/>
<wire x1="21.095" y1="0.89" x2="22.23" y2="1.525" width="0.1524" layer="21"/>
<wire x1="22.23" y1="2.795" x2="22.23" y2="1.525" width="0.1524" layer="21"/>
<wire x1="20.325" y1="-4.19" x2="21.595" y2="-4.19" width="0.1524" layer="21"/>
<wire x1="20.325" y1="-4.19" x2="19.19" y2="-3.555" width="0.1524" layer="21"/>
<wire x1="19.19" y1="-3.555" x2="19.19" y2="-2.285" width="0.1524" layer="21"/>
<wire x1="19.19" y1="-2.285" x2="19.825" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="19.825" y1="-1.65" x2="21.095" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="21.095" y1="-1.65" x2="22.23" y2="-2.285" width="0.1524" layer="21"/>
<wire x1="22.23" y1="-2.285" x2="22.23" y2="-3.555" width="0.1524" layer="21"/>
<wire x1="22.23" y1="-3.555" x2="21.595" y2="-4.19" width="0.1524" layer="21"/>
<pad name="B" x="20.8" y="-2.92" drill="1.016" shape="long" rot="R180"/>
<pad name="NC" x="20.8" y="-0.38" drill="1.016" shape="long" rot="R180"/>
<pad name="A" x="20.8" y="2.16" drill="1.016" shape="long" rot="R180"/>
<wire x1="-21.455" y1="5.97" x2="-22.09" y2="6.605" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="6.605" x2="-22.09" y2="7.875" width="0.1524" layer="21"/>
<wire x1="-22.09" y1="7.875" x2="-21.455" y2="8.51" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="8.51" x2="-20.185" y2="8.51" width="0.1524" layer="21"/>
<wire x1="-20.185" y1="8.51" x2="-19.55" y2="7.875" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="7.875" x2="-19.55" y2="6.605" width="0.1524" layer="21"/>
<wire x1="-19.55" y1="6.605" x2="-20.185" y2="5.97" width="0.1524" layer="21"/>
<wire x1="-21.455" y1="5.97" x2="-20.185" y2="5.97" width="0.1524" layer="21"/>
<pad name="VDD" x="-20.82" y="7.24" drill="1.016" shape="long" rot="R180"/>
<wire x1="-22.5" y1="9" x2="-22.5" y2="-9.5" width="0.1524" layer="21"/>
<wire x1="-22.5" y1="9" x2="-19" y2="9" width="0.1524" layer="21"/>
<wire x1="-19" y1="9" x2="22.5" y2="9" width="0.1524" layer="21"/>
<wire x1="-22.5" y1="-9.5" x2="-19" y2="-9.5" width="0.1524" layer="21"/>
<wire x1="-19" y1="-9.5" x2="22.5" y2="-9.5" width="0.1524" layer="21"/>
<wire x1="22.5" y1="9" x2="22.5" y2="4" width="0.1524" layer="21"/>
<wire x1="22.5" y1="4" x2="22.5" y2="-4.5" width="0.1524" layer="21"/>
<wire x1="22.5" y1="-4.5" x2="22.5" y2="-9.5" width="0.1524" layer="21"/>
<wire x1="-19" y1="9" x2="-19" y2="4" width="0.1524" layer="21"/>
<wire x1="-19" y1="4" x2="-19" y2="-4.5" width="0.1524" layer="21"/>
<wire x1="-19" y1="-4.5" x2="-19" y2="-9.5" width="0.1524" layer="21"/>
<wire x1="-19" y1="4" x2="18.5" y2="4" width="0.1524" layer="21"/>
<wire x1="18.5" y1="4" x2="22.5" y2="4" width="0.1524" layer="21"/>
<wire x1="-19" y1="-4.5" x2="18" y2="-4.5" width="0.1524" layer="21"/>
<wire x1="18.5" y1="-4.5" x2="22.5" y2="-4.5" width="0.1524" layer="21"/>
<wire x1="18.5" y1="4" x2="18.5" y2="-4.5" width="0.1524" layer="21"/>
<wire x1="18.5" y1="-4.5" x2="18" y2="-4.5" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="EC20">
<pin name="VDD" x="-15.24" y="7.62" length="middle" direction="pwr"/>
<pin name="RXD" x="-15.24" y="5.08" length="middle"/>
<pin name="TXD" x="-15.24" y="2.54" length="middle"/>
<pin name="RST" x="-15.24" y="0" length="middle"/>
<pin name="OCD" x="-15.24" y="-2.54" length="middle"/>
<pin name="RTS" x="-15.24" y="-5.08" length="middle"/>
<pin name="GND" x="-15.24" y="-7.62" length="middle"/>
<pin name="B" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="NC" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="A" x="17.78" y="2.54" length="middle" rot="R180"/>
<text x="2.54" y="-2.54" size="1.778" layer="95" rot="R90">EC20</text>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="12.7" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="10.16" width="0.1524" layer="94"/>
<wire x1="12.7" y1="10.16" x2="-10.16" y2="10.16" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HART2012H">
<gates>
<gate name="G$1" symbol="EC20" x="-7.62" y="2.54"/>
</gates>
<devices>
<device name="" package="HAR2012H">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="OCD" pad="OCD"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="RTS" pad="RTS"/>
<connect gate="G$1" pin="RXD" pad="RXD"/>
<connect gate="G$1" pin="TXD" pad="TXD"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="STM32F103C8T6">
<packages>
<package name="QFP50P900X900X160-48N">
<wire x1="-3.21" y1="3.6" x2="-3.6" y2="3.6" width="0.127" layer="21"/>
<wire x1="-3.6" y1="3.6" x2="-3.6" y2="3.21" width="0.127" layer="21"/>
<circle x="-6" y="2.95" radius="0.15" width="0.2" layer="21"/>
<wire x1="-3.6" y1="3.6" x2="3.6" y2="3.6" width="0.127" layer="51"/>
<wire x1="3.6" y1="3.6" x2="3.6" y2="-3.6" width="0.127" layer="51"/>
<wire x1="3.6" y1="-3.6" x2="-3.6" y2="-3.6" width="0.127" layer="51"/>
<wire x1="-3.6" y1="-3.6" x2="-3.6" y2="3.6" width="0.127" layer="51"/>
<wire x1="-3.6" y1="-3.21" x2="-3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.6" y1="-3.6" x2="-3.21" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.21" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-3.21" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.21" x2="3.6" y2="3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.6" x2="3.21" y2="3.6" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3.14" x2="-3.85" y2="3.14" width="0.05" layer="39"/>
<wire x1="-3.85" y1="3.14" x2="-3.85" y2="3.85" width="0.05" layer="39"/>
<wire x1="-3.85" y1="3.85" x2="-3.14" y2="3.85" width="0.05" layer="39"/>
<wire x1="-3.14" y1="3.85" x2="-3.14" y2="5.21" width="0.05" layer="39"/>
<wire x1="-3.14" y1="5.21" x2="3.14" y2="5.21" width="0.05" layer="39"/>
<wire x1="3.14" y1="5.21" x2="3.14" y2="3.85" width="0.05" layer="39"/>
<wire x1="3.14" y1="3.85" x2="3.85" y2="3.85" width="0.05" layer="39"/>
<wire x1="3.85" y1="3.85" x2="3.85" y2="3.14" width="0.05" layer="39"/>
<wire x1="3.85" y1="3.14" x2="5.21" y2="3.14" width="0.05" layer="39"/>
<wire x1="5.21" y1="3.14" x2="5.21" y2="-3.14" width="0.05" layer="39"/>
<wire x1="5.21" y1="-3.14" x2="3.85" y2="-3.14" width="0.05" layer="39"/>
<wire x1="3.85" y1="-3.14" x2="3.85" y2="-3.85" width="0.05" layer="39"/>
<wire x1="3.85" y1="-3.85" x2="3.14" y2="-3.85" width="0.05" layer="39"/>
<wire x1="3.14" y1="-3.85" x2="3.14" y2="-5.21" width="0.05" layer="39"/>
<wire x1="3.14" y1="-5.21" x2="-3.14" y2="-5.21" width="0.05" layer="39"/>
<wire x1="-3.14" y1="-5.21" x2="-3.14" y2="-3.85" width="0.05" layer="39"/>
<wire x1="-3.14" y1="-3.85" x2="-3.85" y2="-3.85" width="0.05" layer="39"/>
<wire x1="-3.85" y1="-3.85" x2="-3.85" y2="-3.14" width="0.05" layer="39"/>
<wire x1="-3.85" y1="-3.14" x2="-5.21" y2="-3.14" width="0.05" layer="39"/>
<wire x1="-5.21" y1="-3.14" x2="-5.21" y2="3.14" width="0.05" layer="39"/>
<text x="-5.5" y="5.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-7" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-6" y="2.95" radius="0.15" width="0.2" layer="51"/>
<smd name="1" x="-4.18" y="2.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="-4.18" y="2.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-4.18" y="1.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="4" x="-4.18" y="1.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="5" x="-4.18" y="0.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="6" x="-4.18" y="0.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="7" x="-4.18" y="-0.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="8" x="-4.18" y="-0.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="9" x="-4.18" y="-1.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="10" x="-4.18" y="-1.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="11" x="-4.18" y="-2.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="12" x="-4.18" y="-2.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="13" x="-2.75" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="14" x="-2.25" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="15" x="-1.75" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="16" x="-1.25" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="17" x="-0.75" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="18" x="-0.25" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="19" x="0.25" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="20" x="0.75" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="21" x="1.25" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="22" x="1.75" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="23" x="2.25" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="24" x="2.75" y="-4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="25" x="4.18" y="-2.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="26" x="4.18" y="-2.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="27" x="4.18" y="-1.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="28" x="4.18" y="-1.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="29" x="4.18" y="-0.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="30" x="4.18" y="-0.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="31" x="4.18" y="0.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="32" x="4.18" y="0.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="33" x="4.18" y="1.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="34" x="4.18" y="1.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="35" x="4.18" y="2.25" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="36" x="4.18" y="2.75" dx="0.28" dy="1.56" layer="1" roundness="50" rot="R90"/>
<smd name="37" x="2.75" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="38" x="2.25" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="39" x="1.75" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="40" x="1.25" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="41" x="0.75" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="42" x="0.25" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="43" x="-0.25" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="44" x="-0.75" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="45" x="-1.25" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="46" x="-1.75" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="47" x="-2.25" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
<smd name="48" x="-2.75" y="4.18" dx="0.28" dy="1.56" layer="1" roundness="50"/>
</package>
</packages>
<symbols>
<symbol name="STM32F103C8T6">
<wire x1="20.32" y1="40.64" x2="20.32" y2="-38.1" width="0.254" layer="94"/>
<wire x1="20.32" y1="-38.1" x2="-20.32" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-38.1" x2="-20.32" y2="40.64" width="0.254" layer="94"/>
<wire x1="-20.32" y1="40.64" x2="20.32" y2="40.64" width="0.254" layer="94"/>
<text x="-20.32" y="41.275" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="-40.64" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VBAT" x="-25.4" y="30.48" length="middle"/>
<pin name="PC13-TAMPER-RTC" x="-25.4" y="-22.86" length="middle"/>
<pin name="PC14-OSC32_IN" x="-25.4" y="-25.4" length="middle"/>
<pin name="PC15-OSC32_OUT" x="-25.4" y="-27.94" length="middle"/>
<pin name="PD0_OSC_IN" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="PD1_OSC_OUT" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="NRST" x="-25.4" y="27.94" length="middle" direction="in"/>
<pin name="VSSA" x="25.4" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDA" x="25.4" y="38.1" length="middle" direction="pwr" rot="R180"/>
<pin name="PA0_WKUP" x="-25.4" y="20.32" length="middle"/>
<pin name="PA1" x="-25.4" y="17.78" length="middle"/>
<pin name="PA2" x="-25.4" y="15.24" length="middle"/>
<pin name="PA3" x="-25.4" y="12.7" length="middle"/>
<pin name="PA4" x="-25.4" y="10.16" length="middle"/>
<pin name="PA5" x="-25.4" y="7.62" length="middle"/>
<pin name="PA6" x="-25.4" y="5.08" length="middle"/>
<pin name="PA7" x="-25.4" y="2.54" length="middle"/>
<pin name="PB0" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PB1" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PB2" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PB10" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PB11" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="VSS" x="25.4" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD" x="25.4" y="35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="PB12" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PB13" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PB14" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PB15" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="PA8" x="-25.4" y="0" length="middle"/>
<pin name="PA9" x="-25.4" y="-2.54" length="middle"/>
<pin name="PA10" x="-25.4" y="-5.08" length="middle"/>
<pin name="PA11" x="-25.4" y="-7.62" length="middle"/>
<pin name="PA12" x="-25.4" y="-10.16" length="middle"/>
<pin name="PA13" x="-25.4" y="-12.7" length="middle"/>
<pin name="PA14" x="-25.4" y="-15.24" length="middle"/>
<pin name="PA15" x="-25.4" y="-17.78" length="middle"/>
<pin name="PB3" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PB4" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PB5" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="PB6" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="PB7" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="BOOT0" x="-25.4" y="25.4" length="middle" direction="in"/>
<pin name="PB8" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PB9" x="25.4" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F103C8T6" prefix="U">
<description>Mainstream Performance line, ARM Cortex-M3 MCU with 64 Kbytes Flash, 72 MHz CPU, motor control, USB and CAN</description>
<gates>
<gate name="G$1" symbol="STM32F103C8T6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P900X900X160-48N">
<connects>
<connect gate="G$1" pin="BOOT0" pad="44"/>
<connect gate="G$1" pin="NRST" pad="7"/>
<connect gate="G$1" pin="PA0_WKUP" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA10" pad="31"/>
<connect gate="G$1" pin="PA11" pad="32"/>
<connect gate="G$1" pin="PA12" pad="33"/>
<connect gate="G$1" pin="PA13" pad="34"/>
<connect gate="G$1" pin="PA14" pad="37"/>
<connect gate="G$1" pin="PA15" pad="38"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3" pad="13"/>
<connect gate="G$1" pin="PA4" pad="14"/>
<connect gate="G$1" pin="PA5" pad="15"/>
<connect gate="G$1" pin="PA6" pad="16"/>
<connect gate="G$1" pin="PA7" pad="17"/>
<connect gate="G$1" pin="PA8" pad="29"/>
<connect gate="G$1" pin="PA9" pad="30"/>
<connect gate="G$1" pin="PB0" pad="18"/>
<connect gate="G$1" pin="PB1" pad="19"/>
<connect gate="G$1" pin="PB10" pad="21"/>
<connect gate="G$1" pin="PB11" pad="22"/>
<connect gate="G$1" pin="PB12" pad="25"/>
<connect gate="G$1" pin="PB13" pad="26"/>
<connect gate="G$1" pin="PB14" pad="27"/>
<connect gate="G$1" pin="PB15" pad="28"/>
<connect gate="G$1" pin="PB2" pad="20"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC13-TAMPER-RTC" pad="2"/>
<connect gate="G$1" pin="PC14-OSC32_IN" pad="3"/>
<connect gate="G$1" pin="PC15-OSC32_OUT" pad="4"/>
<connect gate="G$1" pin="PD0_OSC_IN" pad="5"/>
<connect gate="G$1" pin="PD1_OSC_OUT" pad="6"/>
<connect gate="G$1" pin="VBAT" pad="1"/>
<connect gate="G$1" pin="VDD" pad="24 36 48"/>
<connect gate="G$1" pin="VDDA" pad="9"/>
<connect gate="G$1" pin="VSS" pad="23 35 47"/>
<connect gate="G$1" pin="VSSA" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" ARM® Cortex®-M3 STM32F1 Microcontroller IC 32-Bit 72MHz 64KB _64K x 8_ FLASH "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="STM32F103C8T6"/>
<attribute name="PACKAGE" value="LQFP-48 STMicroelectronics"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames Miota">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC-SPACE">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<rectangle x1="300.2153" y1="11.1125" x2="301.9933" y2="11.2903" layer="94"/>
<rectangle x1="307.3273" y1="11.1125" x2="309.1053" y2="11.2903" layer="94"/>
<rectangle x1="310.7055" y1="11.1125" x2="312.6613" y2="11.2903" layer="94"/>
<rectangle x1="315.3283" y1="11.1125" x2="319.5955" y2="11.2903" layer="94"/>
<rectangle x1="322.7959" y1="11.1125" x2="324.3961" y2="11.2903" layer="94"/>
<rectangle x1="326.7075" y1="11.1125" x2="327.5965" y2="11.2903" layer="94"/>
<rectangle x1="330.4413" y1="11.1125" x2="332.2193" y2="11.2903" layer="94"/>
<rectangle x1="300.2153" y1="11.2903" x2="301.9933" y2="11.4681" layer="94"/>
<rectangle x1="307.3273" y1="11.2903" x2="309.1053" y2="11.4681" layer="94"/>
<rectangle x1="310.7055" y1="11.2903" x2="312.6613" y2="11.4681" layer="94"/>
<rectangle x1="314.7949" y1="11.2903" x2="319.7733" y2="11.4681" layer="94"/>
<rectangle x1="322.7959" y1="11.2903" x2="324.3961" y2="11.4681" layer="94"/>
<rectangle x1="325.6407" y1="11.2903" x2="327.7743" y2="11.4681" layer="94"/>
<rectangle x1="330.4413" y1="11.2903" x2="332.2193" y2="11.4681" layer="94"/>
<rectangle x1="300.2153" y1="11.4681" x2="301.9933" y2="11.6459" layer="94"/>
<rectangle x1="307.3273" y1="11.4681" x2="309.1053" y2="11.6459" layer="94"/>
<rectangle x1="310.7055" y1="11.4681" x2="312.6613" y2="11.6459" layer="94"/>
<rectangle x1="314.6171" y1="11.4681" x2="319.9511" y2="11.6459" layer="94"/>
<rectangle x1="322.7959" y1="11.4681" x2="324.3961" y2="11.6459" layer="94"/>
<rectangle x1="325.6407" y1="11.4681" x2="327.9521" y2="11.6459" layer="94"/>
<rectangle x1="330.4413" y1="11.4681" x2="332.2193" y2="11.6459" layer="94"/>
<rectangle x1="300.2153" y1="11.6459" x2="301.9933" y2="11.8237" layer="94"/>
<rectangle x1="307.3273" y1="11.6459" x2="309.1053" y2="11.8237" layer="94"/>
<rectangle x1="310.7055" y1="11.6459" x2="312.6613" y2="11.8237" layer="94"/>
<rectangle x1="314.6171" y1="11.6459" x2="320.1289" y2="11.8237" layer="94"/>
<rectangle x1="322.7959" y1="11.6459" x2="324.3961" y2="11.8237" layer="94"/>
<rectangle x1="325.8185" y1="11.6459" x2="327.9521" y2="11.8237" layer="94"/>
<rectangle x1="330.4413" y1="11.6459" x2="332.2193" y2="11.8237" layer="94"/>
<rectangle x1="300.2153" y1="11.8237" x2="301.9933" y2="12.0015" layer="94"/>
<rectangle x1="307.3273" y1="11.8237" x2="309.1053" y2="12.0015" layer="94"/>
<rectangle x1="310.7055" y1="11.8237" x2="312.6613" y2="12.0015" layer="94"/>
<rectangle x1="314.6171" y1="11.8237" x2="320.1289" y2="12.0015" layer="94"/>
<rectangle x1="322.7959" y1="11.8237" x2="324.3961" y2="12.0015" layer="94"/>
<rectangle x1="325.9963" y1="11.8237" x2="328.1299" y2="12.0015" layer="94"/>
<rectangle x1="330.4413" y1="11.8237" x2="332.2193" y2="12.0015" layer="94"/>
<rectangle x1="300.2153" y1="12.0015" x2="301.9933" y2="12.1793" layer="94"/>
<rectangle x1="307.3273" y1="12.0015" x2="309.1053" y2="12.1793" layer="94"/>
<rectangle x1="310.7055" y1="12.0015" x2="312.6613" y2="12.1793" layer="94"/>
<rectangle x1="314.6171" y1="12.0015" x2="320.1289" y2="12.1793" layer="94"/>
<rectangle x1="322.7959" y1="12.0015" x2="324.3961" y2="12.1793" layer="94"/>
<rectangle x1="325.9963" y1="12.0015" x2="328.1299" y2="12.1793" layer="94"/>
<rectangle x1="330.4413" y1="12.0015" x2="332.2193" y2="12.1793" layer="94"/>
<rectangle x1="300.2153" y1="12.1793" x2="301.9933" y2="12.3571" layer="94"/>
<rectangle x1="307.3273" y1="12.1793" x2="309.1053" y2="12.3571" layer="94"/>
<rectangle x1="310.7055" y1="12.1793" x2="312.6613" y2="12.3571" layer="94"/>
<rectangle x1="314.6171" y1="12.1793" x2="320.1289" y2="12.3571" layer="94"/>
<rectangle x1="322.7959" y1="12.1793" x2="324.3961" y2="12.3571" layer="94"/>
<rectangle x1="326.1741" y1="12.1793" x2="332.2193" y2="12.3571" layer="94"/>
<rectangle x1="300.2153" y1="12.3571" x2="301.9933" y2="12.5349" layer="94"/>
<rectangle x1="307.3273" y1="12.3571" x2="309.1053" y2="12.5349" layer="94"/>
<rectangle x1="310.7055" y1="12.3571" x2="312.6613" y2="12.5349" layer="94"/>
<rectangle x1="314.6171" y1="12.3571" x2="320.1289" y2="12.5349" layer="94"/>
<rectangle x1="322.7959" y1="12.3571" x2="324.3961" y2="12.5349" layer="94"/>
<rectangle x1="326.1741" y1="12.3571" x2="332.2193" y2="12.5349" layer="94"/>
<rectangle x1="300.2153" y1="12.5349" x2="301.9933" y2="12.7127" layer="94"/>
<rectangle x1="307.3273" y1="12.5349" x2="309.1053" y2="12.7127" layer="94"/>
<rectangle x1="310.7055" y1="12.5349" x2="312.6613" y2="12.7127" layer="94"/>
<rectangle x1="314.6171" y1="12.5349" x2="320.1289" y2="12.7127" layer="94"/>
<rectangle x1="322.7959" y1="12.5349" x2="324.3961" y2="12.7127" layer="94"/>
<rectangle x1="326.3519" y1="12.5349" x2="332.2193" y2="12.7127" layer="94"/>
<rectangle x1="300.2153" y1="12.7127" x2="301.9933" y2="12.8905" layer="94"/>
<rectangle x1="304.4825" y1="12.7127" x2="305.0159" y2="12.8905" layer="94"/>
<rectangle x1="307.3273" y1="12.7127" x2="309.1053" y2="12.8905" layer="94"/>
<rectangle x1="310.7055" y1="12.7127" x2="312.6613" y2="12.8905" layer="94"/>
<rectangle x1="314.6171" y1="12.7127" x2="320.1289" y2="12.8905" layer="94"/>
<rectangle x1="322.7959" y1="12.7127" x2="324.3961" y2="12.8905" layer="94"/>
<rectangle x1="326.5297" y1="12.7127" x2="332.2193" y2="12.8905" layer="94"/>
<rectangle x1="300.2153" y1="12.8905" x2="301.9933" y2="13.0683" layer="94"/>
<rectangle x1="304.1269" y1="12.8905" x2="305.3715" y2="13.0683" layer="94"/>
<rectangle x1="307.3273" y1="12.8905" x2="309.1053" y2="13.0683" layer="94"/>
<rectangle x1="310.7055" y1="12.8905" x2="312.6613" y2="13.0683" layer="94"/>
<rectangle x1="314.6171" y1="12.8905" x2="316.3951" y2="13.0683" layer="94"/>
<rectangle x1="318.3509" y1="12.8905" x2="320.1289" y2="13.0683" layer="94"/>
<rectangle x1="322.7959" y1="12.8905" x2="324.3961" y2="13.0683" layer="94"/>
<rectangle x1="326.5297" y1="12.8905" x2="332.2193" y2="13.0683" layer="94"/>
<rectangle x1="300.2153" y1="13.0683" x2="301.9933" y2="13.2461" layer="94"/>
<rectangle x1="303.7713" y1="13.0683" x2="305.5493" y2="13.2461" layer="94"/>
<rectangle x1="307.3273" y1="13.0683" x2="309.1053" y2="13.2461" layer="94"/>
<rectangle x1="310.7055" y1="13.0683" x2="312.6613" y2="13.2461" layer="94"/>
<rectangle x1="314.6171" y1="13.0683" x2="316.2173" y2="13.2461" layer="94"/>
<rectangle x1="318.5287" y1="13.0683" x2="320.1289" y2="13.2461" layer="94"/>
<rectangle x1="322.7959" y1="13.0683" x2="324.3961" y2="13.2461" layer="94"/>
<rectangle x1="326.7075" y1="13.0683" x2="332.2193" y2="13.2461" layer="94"/>
<rectangle x1="300.2153" y1="13.2461" x2="301.9933" y2="13.4239" layer="94"/>
<rectangle x1="303.5935" y1="13.2461" x2="305.7271" y2="13.4239" layer="94"/>
<rectangle x1="307.3273" y1="13.2461" x2="309.1053" y2="13.4239" layer="94"/>
<rectangle x1="310.7055" y1="13.2461" x2="312.6613" y2="13.4239" layer="94"/>
<rectangle x1="314.6171" y1="13.2461" x2="316.2173" y2="13.4239" layer="94"/>
<rectangle x1="318.5287" y1="13.2461" x2="320.1289" y2="13.4239" layer="94"/>
<rectangle x1="322.7959" y1="13.2461" x2="324.3961" y2="13.4239" layer="94"/>
<rectangle x1="326.8853" y1="13.2461" x2="332.2193" y2="13.4239" layer="94"/>
<rectangle x1="300.2153" y1="13.4239" x2="301.9933" y2="13.6017" layer="94"/>
<rectangle x1="303.4157" y1="13.4239" x2="305.9049" y2="13.6017" layer="94"/>
<rectangle x1="307.3273" y1="13.4239" x2="309.1053" y2="13.6017" layer="94"/>
<rectangle x1="310.7055" y1="13.4239" x2="312.6613" y2="13.6017" layer="94"/>
<rectangle x1="314.6171" y1="13.4239" x2="316.2173" y2="13.6017" layer="94"/>
<rectangle x1="318.5287" y1="13.4239" x2="320.1289" y2="13.6017" layer="94"/>
<rectangle x1="322.7959" y1="13.4239" x2="324.3961" y2="13.6017" layer="94"/>
<rectangle x1="326.8853" y1="13.4239" x2="332.2193" y2="13.6017" layer="94"/>
<rectangle x1="300.2153" y1="13.6017" x2="301.9933" y2="13.7795" layer="94"/>
<rectangle x1="303.4157" y1="13.6017" x2="306.0827" y2="13.7795" layer="94"/>
<rectangle x1="307.3273" y1="13.6017" x2="309.1053" y2="13.7795" layer="94"/>
<rectangle x1="310.7055" y1="13.6017" x2="312.6613" y2="13.7795" layer="94"/>
<rectangle x1="314.6171" y1="13.6017" x2="316.2173" y2="13.7795" layer="94"/>
<rectangle x1="318.5287" y1="13.6017" x2="320.1289" y2="13.7795" layer="94"/>
<rectangle x1="322.7959" y1="13.6017" x2="324.3961" y2="13.7795" layer="94"/>
<rectangle x1="327.0631" y1="13.6017" x2="332.2193" y2="13.7795" layer="94"/>
<rectangle x1="300.2153" y1="13.7795" x2="301.9933" y2="13.9573" layer="94"/>
<rectangle x1="303.2379" y1="13.7795" x2="306.0827" y2="13.9573" layer="94"/>
<rectangle x1="307.3273" y1="13.7795" x2="309.1053" y2="13.9573" layer="94"/>
<rectangle x1="310.7055" y1="13.7795" x2="312.6613" y2="13.9573" layer="94"/>
<rectangle x1="314.6171" y1="13.7795" x2="316.2173" y2="13.9573" layer="94"/>
<rectangle x1="318.5287" y1="13.7795" x2="320.1289" y2="13.9573" layer="94"/>
<rectangle x1="322.7959" y1="13.7795" x2="324.3961" y2="13.9573" layer="94"/>
<rectangle x1="327.2409" y1="13.7795" x2="332.2193" y2="13.9573" layer="94"/>
<rectangle x1="300.2153" y1="13.9573" x2="301.9933" y2="14.1351" layer="94"/>
<rectangle x1="303.0601" y1="13.9573" x2="306.2605" y2="14.1351" layer="94"/>
<rectangle x1="307.3273" y1="13.9573" x2="309.1053" y2="14.1351" layer="94"/>
<rectangle x1="310.7055" y1="13.9573" x2="312.6613" y2="14.1351" layer="94"/>
<rectangle x1="314.6171" y1="13.9573" x2="316.2173" y2="14.1351" layer="94"/>
<rectangle x1="318.5287" y1="13.9573" x2="320.1289" y2="14.1351" layer="94"/>
<rectangle x1="322.7959" y1="13.9573" x2="324.3961" y2="14.1351" layer="94"/>
<rectangle x1="327.2409" y1="13.9573" x2="329.3745" y2="14.1351" layer="94"/>
<rectangle x1="300.2153" y1="14.1351" x2="301.9933" y2="14.3129" layer="94"/>
<rectangle x1="302.8823" y1="14.1351" x2="306.4383" y2="14.3129" layer="94"/>
<rectangle x1="307.3273" y1="14.1351" x2="309.1053" y2="14.3129" layer="94"/>
<rectangle x1="310.7055" y1="14.1351" x2="312.6613" y2="14.3129" layer="94"/>
<rectangle x1="314.6171" y1="14.1351" x2="316.2173" y2="14.3129" layer="94"/>
<rectangle x1="318.5287" y1="14.1351" x2="320.1289" y2="14.3129" layer="94"/>
<rectangle x1="322.7959" y1="14.1351" x2="324.3961" y2="14.3129" layer="94"/>
<rectangle x1="327.4187" y1="14.1351" x2="329.5523" y2="14.3129" layer="94"/>
<rectangle x1="302.8823" y1="14.3129" x2="306.4383" y2="14.4907" layer="94"/>
<rectangle x1="310.7055" y1="14.3129" x2="312.6613" y2="14.4907" layer="94"/>
<rectangle x1="314.6171" y1="14.3129" x2="316.2173" y2="14.4907" layer="94"/>
<rectangle x1="318.5287" y1="14.3129" x2="320.1289" y2="14.4907" layer="94"/>
<rectangle x1="322.7959" y1="14.3129" x2="324.3961" y2="14.4907" layer="94"/>
<rectangle x1="327.4187" y1="14.3129" x2="329.5523" y2="14.4907" layer="94"/>
<rectangle x1="330.4413" y1="14.3129" x2="330.6191" y2="14.4907" layer="94"/>
<rectangle x1="302.7045" y1="14.4907" x2="306.6161" y2="14.6685" layer="94"/>
<rectangle x1="310.7055" y1="14.4907" x2="312.6613" y2="14.6685" layer="94"/>
<rectangle x1="314.6171" y1="14.4907" x2="316.2173" y2="14.6685" layer="94"/>
<rectangle x1="318.5287" y1="14.4907" x2="320.1289" y2="14.6685" layer="94"/>
<rectangle x1="322.7959" y1="14.4907" x2="324.3961" y2="14.6685" layer="94"/>
<rectangle x1="327.5965" y1="14.4907" x2="329.7301" y2="14.6685" layer="94"/>
<rectangle x1="330.4413" y1="14.4907" x2="332.2193" y2="14.6685" layer="94"/>
<rectangle x1="302.7045" y1="14.6685" x2="306.7939" y2="14.8463" layer="94"/>
<rectangle x1="310.7055" y1="14.6685" x2="312.6613" y2="14.8463" layer="94"/>
<rectangle x1="314.6171" y1="14.6685" x2="316.2173" y2="14.8463" layer="94"/>
<rectangle x1="318.5287" y1="14.6685" x2="320.1289" y2="14.8463" layer="94"/>
<rectangle x1="322.7959" y1="14.6685" x2="324.3961" y2="14.8463" layer="94"/>
<rectangle x1="327.7743" y1="14.6685" x2="329.7301" y2="14.8463" layer="94"/>
<rectangle x1="330.4413" y1="14.6685" x2="332.2193" y2="14.8463" layer="94"/>
<rectangle x1="300.2153" y1="14.8463" x2="301.9933" y2="15.0241" layer="94"/>
<rectangle x1="302.5267" y1="14.8463" x2="306.7939" y2="15.0241" layer="94"/>
<rectangle x1="307.3273" y1="14.8463" x2="309.1053" y2="15.0241" layer="94"/>
<rectangle x1="310.7055" y1="14.8463" x2="312.6613" y2="15.0241" layer="94"/>
<rectangle x1="314.6171" y1="14.8463" x2="316.2173" y2="15.0241" layer="94"/>
<rectangle x1="318.5287" y1="14.8463" x2="320.1289" y2="15.0241" layer="94"/>
<rectangle x1="322.7959" y1="14.8463" x2="324.3961" y2="15.0241" layer="94"/>
<rectangle x1="327.7743" y1="14.8463" x2="329.9079" y2="15.0241" layer="94"/>
<rectangle x1="330.4413" y1="14.8463" x2="332.2193" y2="15.0241" layer="94"/>
<rectangle x1="300.2153" y1="15.0241" x2="301.9933" y2="15.2019" layer="94"/>
<rectangle x1="302.3489" y1="15.0241" x2="306.9717" y2="15.2019" layer="94"/>
<rectangle x1="307.3273" y1="15.0241" x2="309.1053" y2="15.2019" layer="94"/>
<rectangle x1="310.7055" y1="15.0241" x2="312.6613" y2="15.2019" layer="94"/>
<rectangle x1="314.6171" y1="15.0241" x2="316.2173" y2="15.2019" layer="94"/>
<rectangle x1="318.5287" y1="15.0241" x2="320.1289" y2="15.2019" layer="94"/>
<rectangle x1="322.7959" y1="15.0241" x2="324.3961" y2="15.2019" layer="94"/>
<rectangle x1="327.9521" y1="15.0241" x2="330.0857" y2="15.2019" layer="94"/>
<rectangle x1="330.4413" y1="15.0241" x2="332.2193" y2="15.2019" layer="94"/>
<rectangle x1="300.2153" y1="15.2019" x2="301.9933" y2="15.3797" layer="94"/>
<rectangle x1="302.3489" y1="15.2019" x2="306.9717" y2="15.3797" layer="94"/>
<rectangle x1="307.3273" y1="15.2019" x2="309.1053" y2="15.3797" layer="94"/>
<rectangle x1="310.7055" y1="15.2019" x2="312.6613" y2="15.3797" layer="94"/>
<rectangle x1="314.6171" y1="15.2019" x2="316.2173" y2="15.3797" layer="94"/>
<rectangle x1="318.5287" y1="15.2019" x2="320.1289" y2="15.3797" layer="94"/>
<rectangle x1="322.7959" y1="15.2019" x2="324.3961" y2="15.3797" layer="94"/>
<rectangle x1="327.9521" y1="15.2019" x2="330.2635" y2="15.3797" layer="94"/>
<rectangle x1="330.4413" y1="15.2019" x2="332.2193" y2="15.3797" layer="94"/>
<rectangle x1="300.2153" y1="15.3797" x2="301.9933" y2="15.5575" layer="94"/>
<rectangle x1="302.1711" y1="15.3797" x2="304.4825" y2="15.5575" layer="94"/>
<rectangle x1="304.8381" y1="15.3797" x2="306.9717" y2="15.5575" layer="94"/>
<rectangle x1="307.3273" y1="15.3797" x2="309.1053" y2="15.5575" layer="94"/>
<rectangle x1="310.7055" y1="15.3797" x2="312.6613" y2="15.5575" layer="94"/>
<rectangle x1="314.6171" y1="15.3797" x2="316.2173" y2="15.5575" layer="94"/>
<rectangle x1="318.5287" y1="15.3797" x2="320.1289" y2="15.5575" layer="94"/>
<rectangle x1="322.7959" y1="15.3797" x2="324.3961" y2="15.5575" layer="94"/>
<rectangle x1="328.1299" y1="15.3797" x2="332.2193" y2="15.5575" layer="94"/>
<rectangle x1="300.2153" y1="15.5575" x2="304.3047" y2="15.7353" layer="94"/>
<rectangle x1="304.8381" y1="15.5575" x2="307.1495" y2="15.7353" layer="94"/>
<rectangle x1="307.3273" y1="15.5575" x2="309.1053" y2="15.7353" layer="94"/>
<rectangle x1="310.7055" y1="15.5575" x2="312.6613" y2="15.7353" layer="94"/>
<rectangle x1="314.6171" y1="15.5575" x2="316.2173" y2="15.7353" layer="94"/>
<rectangle x1="318.5287" y1="15.5575" x2="320.1289" y2="15.7353" layer="94"/>
<rectangle x1="322.7959" y1="15.5575" x2="324.3961" y2="15.7353" layer="94"/>
<rectangle x1="328.3077" y1="15.5575" x2="332.2193" y2="15.7353" layer="94"/>
<rectangle x1="300.2153" y1="15.7353" x2="304.3047" y2="15.9131" layer="94"/>
<rectangle x1="305.0159" y1="15.7353" x2="309.1053" y2="15.9131" layer="94"/>
<rectangle x1="310.7055" y1="15.7353" x2="312.6613" y2="15.9131" layer="94"/>
<rectangle x1="314.6171" y1="15.7353" x2="316.2173" y2="15.9131" layer="94"/>
<rectangle x1="318.5287" y1="15.7353" x2="320.1289" y2="15.9131" layer="94"/>
<rectangle x1="322.7959" y1="15.7353" x2="324.3961" y2="15.9131" layer="94"/>
<rectangle x1="328.4855" y1="15.7353" x2="332.2193" y2="15.9131" layer="94"/>
<rectangle x1="300.2153" y1="15.9131" x2="304.3047" y2="16.0909" layer="94"/>
<rectangle x1="305.0159" y1="15.9131" x2="309.1053" y2="16.0909" layer="94"/>
<rectangle x1="310.7055" y1="15.9131" x2="312.6613" y2="16.0909" layer="94"/>
<rectangle x1="322.7959" y1="15.9131" x2="324.3961" y2="16.0909" layer="94"/>
<rectangle x1="328.4855" y1="15.9131" x2="332.2193" y2="16.0909" layer="94"/>
<rectangle x1="300.2153" y1="16.0909" x2="304.1269" y2="16.2687" layer="94"/>
<rectangle x1="305.1937" y1="16.0909" x2="309.1053" y2="16.2687" layer="94"/>
<rectangle x1="310.7055" y1="16.0909" x2="310.8833" y2="16.2687" layer="94"/>
<rectangle x1="322.7959" y1="16.0909" x2="324.3961" y2="16.2687" layer="94"/>
<rectangle x1="328.6633" y1="16.0909" x2="332.2193" y2="16.2687" layer="94"/>
<rectangle x1="300.2153" y1="16.2687" x2="303.9491" y2="16.4465" layer="94"/>
<rectangle x1="305.3715" y1="16.2687" x2="309.1053" y2="16.4465" layer="94"/>
<rectangle x1="322.7959" y1="16.2687" x2="324.3961" y2="16.4465" layer="94"/>
<rectangle x1="328.6633" y1="16.2687" x2="332.2193" y2="16.4465" layer="94"/>
<rectangle x1="300.2153" y1="16.4465" x2="303.9491" y2="16.6243" layer="94"/>
<rectangle x1="305.3715" y1="16.4465" x2="309.1053" y2="16.6243" layer="94"/>
<rectangle x1="310.8833" y1="16.4465" x2="312.6613" y2="16.6243" layer="94"/>
<rectangle x1="314.6171" y1="16.4465" x2="320.1289" y2="16.6243" layer="94"/>
<rectangle x1="322.7959" y1="16.4465" x2="324.3961" y2="16.6243" layer="94"/>
<rectangle x1="328.8411" y1="16.4465" x2="332.2193" y2="16.6243" layer="94"/>
<rectangle x1="300.2153" y1="16.6243" x2="303.7713" y2="16.8021" layer="94"/>
<rectangle x1="305.5493" y1="16.6243" x2="309.1053" y2="16.8021" layer="94"/>
<rectangle x1="310.7055" y1="16.6243" x2="312.6613" y2="16.8021" layer="94"/>
<rectangle x1="314.6171" y1="16.6243" x2="320.1289" y2="16.8021" layer="94"/>
<rectangle x1="321.0179" y1="16.6243" x2="324.3961" y2="16.8021" layer="94"/>
<rectangle x1="324.9295" y1="16.6243" x2="326.1741" y2="16.8021" layer="94"/>
<rectangle x1="329.0189" y1="16.6243" x2="332.2193" y2="16.8021" layer="94"/>
<rectangle x1="300.2153" y1="16.8021" x2="303.7713" y2="16.9799" layer="94"/>
<rectangle x1="305.7271" y1="16.8021" x2="309.1053" y2="16.9799" layer="94"/>
<rectangle x1="310.7055" y1="16.8021" x2="312.6613" y2="16.9799" layer="94"/>
<rectangle x1="314.6171" y1="16.8021" x2="320.1289" y2="16.9799" layer="94"/>
<rectangle x1="321.0179" y1="16.8021" x2="324.3961" y2="16.9799" layer="94"/>
<rectangle x1="324.9295" y1="16.8021" x2="326.3519" y2="16.9799" layer="94"/>
<rectangle x1="329.0189" y1="16.8021" x2="332.2193" y2="16.9799" layer="94"/>
<rectangle x1="300.2153" y1="16.9799" x2="303.5935" y2="17.1577" layer="94"/>
<rectangle x1="305.7271" y1="16.9799" x2="309.1053" y2="17.1577" layer="94"/>
<rectangle x1="310.7055" y1="16.9799" x2="312.6613" y2="17.1577" layer="94"/>
<rectangle x1="314.6171" y1="16.9799" x2="320.1289" y2="17.1577" layer="94"/>
<rectangle x1="321.0179" y1="16.9799" x2="324.3961" y2="17.1577" layer="94"/>
<rectangle x1="324.9295" y1="16.9799" x2="326.3519" y2="17.1577" layer="94"/>
<rectangle x1="329.1967" y1="16.9799" x2="332.2193" y2="17.1577" layer="94"/>
<rectangle x1="300.2153" y1="17.1577" x2="303.4157" y2="17.3355" layer="94"/>
<rectangle x1="305.9049" y1="17.1577" x2="309.1053" y2="17.3355" layer="94"/>
<rectangle x1="310.7055" y1="17.1577" x2="312.6613" y2="17.3355" layer="94"/>
<rectangle x1="314.6171" y1="17.1577" x2="320.1289" y2="17.3355" layer="94"/>
<rectangle x1="321.0179" y1="17.1577" x2="324.3961" y2="17.3355" layer="94"/>
<rectangle x1="324.9295" y1="17.1577" x2="326.5297" y2="17.3355" layer="94"/>
<rectangle x1="329.3745" y1="17.1577" x2="332.2193" y2="17.3355" layer="94"/>
<rectangle x1="300.2153" y1="17.3355" x2="303.4157" y2="17.5133" layer="94"/>
<rectangle x1="305.9049" y1="17.3355" x2="309.1053" y2="17.5133" layer="94"/>
<rectangle x1="310.7055" y1="17.3355" x2="312.6613" y2="17.5133" layer="94"/>
<rectangle x1="314.6171" y1="17.3355" x2="320.1289" y2="17.5133" layer="94"/>
<rectangle x1="321.0179" y1="17.3355" x2="324.3961" y2="17.5133" layer="94"/>
<rectangle x1="324.9295" y1="17.3355" x2="326.5297" y2="17.5133" layer="94"/>
<rectangle x1="329.5523" y1="17.3355" x2="332.0415" y2="17.5133" layer="94"/>
<rectangle x1="300.2153" y1="17.5133" x2="303.2379" y2="17.6911" layer="94"/>
<rectangle x1="306.0827" y1="17.5133" x2="309.1053" y2="17.6911" layer="94"/>
<rectangle x1="310.7055" y1="17.5133" x2="312.6613" y2="17.6911" layer="94"/>
<rectangle x1="314.6171" y1="17.5133" x2="320.1289" y2="17.6911" layer="94"/>
<rectangle x1="321.0179" y1="17.5133" x2="324.3961" y2="17.6911" layer="94"/>
<rectangle x1="324.9295" y1="17.5133" x2="326.7075" y2="17.6911" layer="94"/>
<rectangle x1="329.5523" y1="17.5133" x2="332.0415" y2="17.6911" layer="94"/>
<rectangle x1="300.2153" y1="17.6911" x2="303.0601" y2="17.8689" layer="94"/>
<rectangle x1="306.0827" y1="17.6911" x2="308.9275" y2="17.8689" layer="94"/>
<rectangle x1="310.7055" y1="17.6911" x2="312.6613" y2="17.8689" layer="94"/>
<rectangle x1="314.6171" y1="17.6911" x2="320.1289" y2="17.8689" layer="94"/>
<rectangle x1="321.0179" y1="17.6911" x2="324.3961" y2="17.8689" layer="94"/>
<rectangle x1="324.9295" y1="17.6911" x2="326.8853" y2="17.8689" layer="94"/>
<rectangle x1="329.7301" y1="17.6911" x2="332.0415" y2="17.8689" layer="94"/>
<rectangle x1="300.3931" y1="17.8689" x2="302.8823" y2="18.0467" layer="94"/>
<rectangle x1="306.2605" y1="17.8689" x2="308.9275" y2="18.0467" layer="94"/>
<rectangle x1="310.7055" y1="17.8689" x2="312.6613" y2="18.0467" layer="94"/>
<rectangle x1="314.6171" y1="17.8689" x2="319.9511" y2="18.0467" layer="94"/>
<rectangle x1="321.0179" y1="17.8689" x2="324.3961" y2="18.0467" layer="94"/>
<rectangle x1="324.9295" y1="17.8689" x2="327.0631" y2="18.0467" layer="94"/>
<rectangle x1="329.7301" y1="17.8689" x2="331.8637" y2="18.0467" layer="94"/>
<rectangle x1="300.3931" y1="18.0467" x2="302.8823" y2="18.2245" layer="94"/>
<rectangle x1="306.4383" y1="18.0467" x2="308.7497" y2="18.2245" layer="94"/>
<rectangle x1="310.7055" y1="18.0467" x2="312.6613" y2="18.2245" layer="94"/>
<rectangle x1="314.7949" y1="18.0467" x2="319.9511" y2="18.2245" layer="94"/>
<rectangle x1="321.0179" y1="18.0467" x2="324.3961" y2="18.2245" layer="94"/>
<rectangle x1="324.9295" y1="18.0467" x2="327.0631" y2="18.2245" layer="94"/>
<rectangle x1="329.9079" y1="18.0467" x2="331.8637" y2="18.2245" layer="94"/>
<rectangle x1="300.5709" y1="18.2245" x2="302.5267" y2="18.4023" layer="94"/>
<rectangle x1="306.6161" y1="18.2245" x2="308.7497" y2="18.4023" layer="94"/>
<rectangle x1="310.7055" y1="18.2245" x2="312.4835" y2="18.4023" layer="94"/>
<rectangle x1="314.9727" y1="18.2245" x2="319.5955" y2="18.4023" layer="94"/>
<rectangle x1="321.0179" y1="18.2245" x2="324.3961" y2="18.4023" layer="94"/>
<rectangle x1="324.9295" y1="18.2245" x2="327.2409" y2="18.4023" layer="94"/>
<rectangle x1="330.4413" y1="18.2245" x2="331.5081" y2="18.4023" layer="94"/>
<rectangle x1="300.7487" y1="18.4023" x2="302.3489" y2="18.5801" layer="94"/>
<rectangle x1="306.9717" y1="18.4023" x2="308.3941" y2="18.5801" layer="94"/>
<rectangle x1="301.1043" y1="18.5801" x2="301.9933" y2="18.7579" layer="94"/>
<rectangle x1="307.3273" y1="18.5801" x2="308.0385" y2="18.7579" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC-SPACE" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC-SPACE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="EC20" deviceset="EC20" device=""/>
<part name="U$2" library="hart2012h" deviceset="HART2012H" device=""/>
<part name="U1" library="STM32F103C8T6" deviceset="STM32F103C8T6" device=""/>
<part name="FRAME1" library="frames Miota" deviceset="A3L-LOC-SPACE" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="284.48" y="170.18" smashed="yes"/>
<instance part="U$2" gate="G$1" x="294.64" y="86.36" smashed="yes"/>
<instance part="U1" gate="G$1" x="121.92" y="137.16" smashed="yes">
<attribute name="NAME" x="101.6" y="178.435" size="1.778" layer="95"/>
<attribute name="VALUE" x="101.6" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
